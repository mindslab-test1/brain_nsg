import argparse, json, time, shutil
from pathlib import Path

import tensorflow as tf
from tensorflow.core.protobuf import rewriter_config_pb2

from components import tokenization, optimization, model, accumulate, sample, utils, load_dataset

parser = argparse.ArgumentParser(description='Fine-tune GPT-2 on your custom dataset.',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('--run_name', type=str, default='run1',
                    help='Run id. Name of subdirectory in checkpoint/ and samples/')
parser.add_argument('--dataset', metavar='PATH', type=str, required=True,
                    help='Input file, directory, or glob pattern (utf-8 text, or preencoded .npz files).')
parser.add_argument('--max_step', type=int, default=20000,
                    help='Maximum training step')
parser.add_argument('--save_every', metavar='N', type=int, default=3000,
                    help='Write a checkpoint every N steps')

parser.add_argument('--batch_size', metavar='SIZE', type=int, default=1,
                    help='Batch size')
parser.add_argument('--accumulate_gradients', metavar='N', type=int, default=1, 
                    help='Accumulate gradients across N minibatches.')
parser.add_argument('--learning_rate', metavar='LR', type=float, default=0.0001,
                    help='Learning rate for Adam')
parser.add_argument('--optimizer', type=str, default='adam',
                    help='Optimizer. <adam|adamW|sgd>.')
parser.add_argument('--input_length', type=int, default=1023,
                    help='Sample this many tokens for input')
parser.add_argument('--noise', type=float, default=0.0,
                    help='Add noise to input training data to regularize against typos.')

parser.add_argument('--sample_every', metavar='N', type=int, default=100,
                    help='Generate samples every N steps')
parser.add_argument('--sample_length', metavar='TOKENS', type=int, default=1023,
                    help='Sample this many tokens for sampling')
parser.add_argument('--sample_count', metavar='N', type=int, default=2,
                    help='Generate this many samples')
parser.add_argument('--sample_top_k', type=int, default=10,
                    help='K for top-k sampling.')
parser.add_argument('--sample_top_p', type=float, default=0,
                    help='P for top-p sampling. Overrides top_k if set > 0.')

parser.add_argument('--memory_saving_gradients', default=False, action='store_true', 
                    help='Use gradient checkpointing to reduce vram usage.')
parser.add_argument('--only_train_transformer_layers', default=False, action='store_true',
                    help='Restrict training to the transformer blocks.')

PRETRAINED_PATH = Path('/pretrained/gpt2_kor')
HPARAMS_PATH = PRETRAINED_PATH.joinpath('hparams.json')
VOCAB_PATH = PRETRAINED_PATH.joinpath('mecab_vocab_feq_top20k.txt')
MODEL_PATH = Path('models')


def main():
    args = parser.parse_args()

    model_dir_path = MODEL_PATH.joinpath(args.run_name)
    model_dir_path.mkdir(exist_ok=True, parents=True)
    log_dir_path = model_dir_path.joinpath('log')
    output_dir_path = model_dir_path.joinpath('output')

    hparams = model.default_hparams()
    with HPARAMS_PATH.open('r') as f:
        hparams.override_from_dict(json.load(f))

    shutil.copy(str(HPARAMS_PATH), str(model_dir_path))
    shutil.copy(str(VOCAB_PATH), str(model_dir_path.joinpath('vocab.txt')))

    if args.sample_length > hparams.n_ctx:
        raise ValueError("Can't get samples longer than window size: %s" % hparams.n_ctx)

    tokenizer = tokenization.FullTokenizer(vocab_file=str(VOCAB_PATH), do_lower_case=False)

    config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
    config.graph_options.rewrite_options.layout_optimizer = rewriter_config_pb2.RewriterConfig.OFF
    with tf.Session(config=config) as sess:
        context = tf.placeholder(tf.int32, [args.batch_size, None])
        context_in = add_noise(context, hparams, args.noise)
        output = model.model(hparams=hparams, X=context_in)
        loss = tf.reduce_mean(
            tf.nn.sparse_softmax_cross_entropy_with_logits(
                labels=context[:, 1:], logits=output['logits'][:, :-1]))

        tf_sample = sample.sample_sequence(
            hparams=hparams,
            length=args.sample_length,
            context=context,
            batch_size=args.batch_size,
            temperature=1.0,
            top_k=args.sample_top_k,
            top_p=args.sample_top_p)

        all_vars = [v for v in tf.trainable_variables() if 'model' in v.name]
        train_vars = [v for v in all_vars if '/h' in v.name] if args.only_train_transformer_layers else all_vars

        if args.optimizer == 'adam':
            opt = tf.train.AdamOptimizer(learning_rate=args.learning_rate)
        elif args.optimizer == 'adamW':
            opt = tf.contrib.opt.AdamWOptimizer(weight_decay=0.01, learning_rate=args.learning_rate)
        elif args.optimizer == 'sgd':
            opt = tf.train.GradientDescentOptimizer(learning_rate=args.learning_rate)
        else:
            exit('optimizer:', args.optimizer)

        if args.accumulate_gradients > 1:
            if args.memory_saving_gradients:
                exit("Memory saving gradients are not implemented for gradient accumulation yet.")
            opt = accumulate.AccumulatingOptimizer(
                opt=opt,
                var_list=train_vars)
            opt_reset = opt.reset()
            opt_compute = opt.compute_gradients(loss)
            opt_apply = opt.apply_gradients()
            summary_loss = tf.summary.scalar('loss', opt_apply)
        else:
            if args.memory_saving_gradients:
                opt_grads = memory_saving_gradients.gradients(loss, train_vars)
            else:
                opt_grads = tf.gradients(loss, train_vars)
            opt_grads = list(zip(opt_grads, train_vars))
            opt_apply = opt.apply_gradients(opt_grads)
            summary_loss = tf.summary.scalar('loss', loss)

        summary_lr = tf.summary.scalar('learning_rate', args.learning_rate)
        summaries = tf.summary.merge([summary_lr, summary_loss])

        summary_log = tf.summary.FileWriter(str(log_dir_path))

        saver = tf.train.Saver(
            var_list=all_vars,
            max_to_keep=10)

        sess.run(tf.global_variables_initializer())
        ckpt = tf.train.latest_checkpoint(str(model_dir_path))
        if ckpt is None:
            ckpt = tf.train.latest_checkpoint(str(PRETRAINED_PATH))
        print('Loading checkpoint from', ckpt)

        if ckpt == None:
            print('ckpk is None.')
        else:
            saver.restore(sess, ckpt)

        print('Loading dataset...')
        data_sampler = load_dataset.Sampler_txt_cut(args.dataset, tokenizer)

        print('Training...')
        train_step = 1
        train_step_path = model_dir_path.joinpath('train_step')
        if train_step_path.exists():
            # Load the step number if we're resuming a run
            # Add 1 so we don't immediately try to save again
            with train_step_path.open('r') as fp:
                train_step = int(fp.read()) + 1

        def save():
            print('Saving', model_dir_path.joinpath('model-{}'.format(train_step)))
            saver.save(sess, str(model_dir_path.joinpath('model')), global_step=train_step)
            with train_step_path.open('w') as fp:
                fp.write(str(train_step) + '\n')

        def generate_samples():
            print('Generating samples...')
            context_tokens = data_sampler.sample(None, True)
            all_text = []
            index = 0
            while index < args.sample_count:
                out = sess.run(
                    tf_sample,
                    feed_dict={context: args.batch_size * [context_tokens]})
                for i in range(min(args.sample_count - index, args.batch_size)):
                    text = tokenizer.convert_ids_to_tokens(out[i])
                    text = '======== SAMPLE {} ========\n{}\n'.format(
                        index + 1, utils.merge_wordpieces(text))
                    all_text.append(text)
                    index += 1
            print(text)
            with output_dir_path.joinpath('samples-{}'.format(train_step)).open('w') as fp:
                fp.write('\n'.join(all_text))

        def sample_batch():
            return [data_sampler.sample(args.input_length) for _ in range(args.batch_size)]

        avg_loss = (0.0, 0.0)
        start_time = time.time()

        try:
            while train_step < args.max_step:
                if train_step % args.save_every == 0:
                    save()
                if train_step % args.sample_every == 0:
                    generate_samples()

                if args.accumulate_gradients > 1:
                    sess.run(opt_reset)
                    for _ in range(args.accumulate_gradients):
                        sess.run(opt_compute, feed_dict={context: sample_batch()})
                    (v_loss, v_summary) = sess.run((opt_apply, summaries))
                else:
                    (_, v_loss, v_summary) = sess.run(
                        (opt_apply, loss, summaries),
                        feed_dict={context: sample_batch()})

                summary_log.add_summary(v_summary, train_step)

                avg_loss = (avg_loss[0] * 0.99 + v_loss,
                            avg_loss[1] * 0.99 + 1.0)

                print('[step: {train_step} | {time:2.2f} elapsed] loss={loss:2.5f} avg={avg:2.5f}'.format(
                        train_step=train_step,
                        time=time.time() - start_time,
                        loss=v_loss,
                        avg=avg_loss[0] / avg_loss[1]))

                train_step += 1
        except KeyboardInterrupt:
            print('interrupted')
            save()

def add_noise(context, hparams, rate):
    if rate > 0:
        mask = tf.random.uniform(shape=tf.shape(context)) < rate
        noise = tf.random.uniform(shape=tf.shape(context), minval=0, maxval=hparams.n_vocab, dtype=tf.int32)
        return tf.where(mask, noise, context)
    else:
        return context


if __name__ == '__main__':
    main()
