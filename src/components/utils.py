import re

ending_chars = '[다요죠오네나라자아"][\.]+'
ending_marks = '[!?][\.]*'
ending_exceptions = '["\']'
whitespaces = '[ \n\t\*]'
EOS = '<<EOS>>'

def convert_text(text):
    '''
    convert text for sentence split
    split token: '<EOS> '
    '''
    out = re.sub('(?P<ending>{})(?!{})[ ]*'.format(ending_chars, ending_exceptions), '\g<ending>{}'.format(EOS),
        re.sub('(?P<ending>{})(?!{})[ ]*'.format(ending_marks, ending_exceptions), '\g<ending>{}'.format(EOS),
        re.sub('[ ]+', ' ', 
        re.sub('{}+'.format(whitespaces), ' ', text))))
    return out.strip()


def split_sentences_kr(text):
    sents = convert_text(text).split(EOS)
    return sents[:-1] if len(sents) > 1 and sents[-1] == '' else sents


# Restore original text from wordpiece tokens
def merge_wordpieces(tokens):
    text = ""
    for i in ' '.join(tokens).replace(' ##', '').split():
        if re.match('\W', i) is not None and re.match('\s', i) is None:
            text += i
        else:
            text += (' '+i)
    for j in ['[', '‘', '{', '<', '(', '“']:
        text = text.replace(j+' ', ' '+j)
    text = ' '.join(['"'+i.strip()+'"' if idx % 2 != 0 else i.strip() for idx, i in enumerate(text.split('"')) ])
    text = ' '.join(["'"+i.strip()+"'" if idx % 2 != 0 else i.strip() for idx, i in enumerate(text.split("'")) ])
    text = text.replace(' [UNK] ', '').replace('[UNK] ', '').replace(' [UNK]', '').replace('[UNK]', '')
    # text = text.replace('>', '')
    return text
