import random
import numpy as np
import os, re
import pickle
from tqdm import tqdm

from components import utils

class Sampler_txt_cut(object):
    def __init__(self, dir, tokenizer):
        self.dir = dir
        txt_file_list = []
        for txt_file in os.listdir(dir):
            if txt_file.endswith('.txt'):
                with open(os.path.join(self.dir, txt_file), 'r') as f:
                    txt_file_list.append(txt_file) if f.read().strip() != '' else print(txt_file, 'is empty file')
        self.txt_file_list = txt_file_list
        self.tokenizer = tokenizer

    def sample(self, length, is_sample=None):
        return self._token_sample_appended(length, is_sample)

    def _token_sample(self, length, is_sample=False):
        selected_file = np.random.choice(self.txt_file_list, 1).item()
        print(selected_file, end=' |  ')
        
        try:
            read_selected_file = open(os.path.join(self.dir, selected_file)).read()
        except:
            read_selected_file = open(os.path.join(self.dir, selected_file), encoding='cp949').read()

        if not is_sample:
            # read_selected_file = re.sub('[\s\n]+', ' ', ' '.join(read_selected_file.split('\n\n')[1:])).strip()
        
            file_ids = self.tokenizer.convert_tokens_to_ids(self.tokenizer.tokenize(read_selected_file))
            while 1:
                if len(file_ids) > length:
                    break
                else:
                    file_ids += file_ids

            index = np.random.randint(0, len(file_ids) - length + 1)
            ret = file_ids[index:length + index]
        else:
            # sample_sents = [re.sub('[\s\n]+', ' ', i).strip()
                            # for i in read_selected_file.split('\n\n') if len(re.sub('[\s\n]+', ' ', i).strip()) > 0]
            sample_sents = read_selected_file
            file_ids = self.tokenizer.convert_tokens_to_ids(self.tokenizer.tokenize(sample_sents))
            ret = file_ids

        return ret

    def _token_sample_appended(self, length, is_sample=False):
        selected_file = np.random.choice(self.txt_file_list, 1).item()
        # print(selected_file, end=' |  ')
        
        try:
            read_selected_file = open(os.path.join(self.dir, selected_file)).read()
        except:
            read_selected_file = open(os.path.join(self.dir, selected_file), encoding='cp949').read()

        if not is_sample:
            # read_selected_file = re.sub('[\s\n]+', ' ', ' '.join(read_selected_file.split('\n\n')[1:])).strip()
        
            ret = self.tokenizer.convert_tokens_to_ids(self.tokenizer.tokenize(read_selected_file))
            while len(ret) < length:
                try: 
                    adding_file = open(os.path.join(self.dir, random.choice(self.txt_file_list)), 'r')
                except ValueError:
                    adding_file = open(os.path.join(self.dir, random.choice(self.txt_file_list)), 'r', encoding='cp949')

                adding_text = adding_file.read()
                ret += self.tokenizer.convert_tokens_to_ids(self.tokenizer.tokenize(adding_text))
                adding_file.close()

            index = np.random.randint(0, len(ret) - length + 1)
            ret = ret[index:length + index]
        else:
            # sample_sents = [re.sub('[\s\n]+', ' ', i).strip()
                            # for i in read_selected_file.split('\n\n') if len(re.sub('[\s\n]+', ' ', i).strip()) > 0]
            sample_sents = read_selected_file
            file_ids = self.tokenizer.convert_tokens_to_ids(self.tokenizer.tokenize(sample_sents))
            ret = file_ids

        return ret

    def _sent_sample(self, length, is_sample=False):
        selected_file = random.choice(self.txt_file_list)
        # print(selected_file, end=' |  ')
        
        try:
            read_selected_file = open(os.path.join(self.dir, selected_file)).read()
        except:
            read_selected_file = open(os.path.join(self.dir, selected_file), encoding='cp949').read()

        if not is_sample:
            sentences = utils.convert_text(read_selected_file).split('.. ')
            sentences_tokenized = [self.tokenizer.tokenize(sent) for sent in sentences if sent is not '']
            index = random.randrange(0, len(sentences_tokenized)-2) if len(sentences_tokenized) > 2 else 0
            sentences_tokenized += sentences_tokenized    

            selected_sents = sentences_tokenized[index:]
            ret = self.tokenizer.convert_tokens_to_ids([token for sent in selected_sents for token in sent])
            ret += [0] * (length - len(ret))
            ret = ret[:length]
        else:
            sample_sents = read_selected_file
            file_ids = self.tokenizer.convert_tokens_to_ids(self.tokenizer.tokenize(sample_sents))
            ret = file_ids

        return ret

    def _sent_sample_appended(self, length, is_sample=False):
        selected_file = random.choice(self.txt_file_list)
        # print(selected_file, end=' |  ')
        
        try:
            read_selected_file = open(os.path.join(self.dir, selected_file)).read()
        except:
            read_selected_file = open(os.path.join(self.dir, selected_file), encoding='cp949').read()

        if not is_sample:
            sentences = utils.convert_text(read_selected_file).split('.. ')
            sentences_tokenized = [self.tokenizer.tokenize(sent) for sent in sentences if sent is not '']
            index = random.randrange(0, len(sentences_tokenized)-2) if len(sentences_tokenized) > 2 else 0
            sentences_tokenized += sentences_tokenized    

            selected_sents = sentences_tokenized[index:]
            ret = self.tokenizer.convert_tokens_to_ids([token for sent in selected_sents for token in sent])

            while len(ret) < length:
                try: 
                    adding_file = open(os.path.join(self.dir, random.choice(self.txt_file_list)), 'r')
                except ValueError:
                    adding_file = open(os.path.join(self.dir, random.choice(self.txt_file_list)), 'r', encoding='cp949')

                adding_text = adding_file.read()
                ret += self.tokenizer.convert_tokens_to_ids(self.tokenizer.tokenize(adding_text))
                adding_file.close()

            ret = ret[:length]
        else:
            sample_sents = read_selected_file
            file_ids = self.tokenizer.convert_tokens_to_ids(self.tokenizer.tokenize(sample_sents))
            ret = file_ids

        return ret
