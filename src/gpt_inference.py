import argparse
import json
import random
import numpy as np
from pathlib import Path

import tensorflow as tf

from components import model, sample, tokenization, utils, load_dataset


class GPT:
    def __init__(self,
                 checkpoint_path,
                 seed=None,
                 temperature=1.0,
                 num_samples=1,
                 batch_size=1,
                 token_count=256,
                 sent_count=None,
                 top_k=5,
                 top_p=0.0):
        """
        Interactively run the model
        :seed=None : Integer seed for random number generators, fix seed to reproduce
         results
        :num_samples=1 : Number of samples to return total
        :batch_size=1 : Number of batches (only affects speed/memory).  Must divide num_samples.
        :token_count=256 : Number of tokens in generated text, if None (default), is
         determined by model hyperparameters
        :sent_count=None : Number of sentence in generated text, if None (default) ignored
        :top_k=0 : Integer value controlling diversity. 1 means only 1 word is
         considered for each step (token), resulting in deterministic completions,
         while 40 means 40 words are considered at each step. 0 (default) is a
         special setting meaning no restrictions. 40 generally is a good value.
        :top_p=0.0 : Float value controlling diversity. Implements nucleus sampling,
         overriding top_k if set to a value > 0. A good setting is 0.9.
        """
        assert num_samples % batch_size == 0
        self.batch_size = batch_size
        self.num_samples = num_samples

        model_path = Path(checkpoint_path)
        model_dir_path = model_path if model_path.is_dir() else model_path.parent
        self.tokenizer = tokenization.FullTokenizer(
            vocab_file=str(model_dir_path.joinpath('vocab.txt')),
            do_lower_case=False)

        hparams = model.default_hparams()
        with model_dir_path.joinpath('hparams.json').open('r') as f:
            hparams.override_from_dict(json.load(f))

        if token_count is None:
            token_count = hparams.n_ctx // 2
        elif token_count > hparams.n_ctx:
            raise ValueError("Can't get samples longer than window size: %s" % hparams.n_ctx)
        self.context = tf.placeholder(tf.int32, [batch_size, None])
        start_token = None

        np.random.seed(seed)
        tf.set_random_seed(seed)

        # for future use
        self.skipping = tf.placeholder(tf.int32, [batch_size])
        
        self.output = sample.sample_sequence(
            hparams=hparams, length=token_count,
            context=self.context,
            start_token=start_token,
            batch_size=batch_size,
            temperature=temperature, top_k=top_k, top_p=top_p)

        self.output_one_step = sample.sample_one_token(
            hparams=hparams, length=token_count,
            context=self.context,
            start_token=start_token,
            skipping=self.skipping,
            batch_size=batch_size,
            temperature=temperature, top_k=top_k, top_p=top_p)

        self.sent_count = sent_count

        self.saver = tf.train.Saver()
        self.sess = tf.Session(config=tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True)))
        self.sess.run(tf.global_variables_initializer())
        self.load_ckpt(checkpoint_path)

    def load_ckpt(self, checkpoint_path):
        ckpt = tf.train.latest_checkpoint(checkpoint_path)
        if ckpt is None:
            ckpt = checkpoint_path
        self.saver.restore(self.sess, ckpt)

    def infer(self, context, num_sentences=1):
        samples = []
        for sample_id in range(self.num_samples):
            sents = []
            cur_context = context
            while len(sents) < num_sentences:
                context_tokens = self.tokenizer.tokenize(cur_context)
                context_ids = self.tokenizer.convert_tokens_to_ids(context_tokens)

                out = self.sess.run(self.output, feed_dict={self.context: [context_ids]})
                out = out[:, len(context_ids):]  # what is this line for?

                out_tokens = self.tokenizer.convert_ids_to_tokens(out[0])
                refined_text = utils.merge_wordpieces(context_tokens + out_tokens)
                new_sents = utils.split_sentences_kr(refined_text)[:self.sent_count]
                new_text = ' '.join(new_sents)
                sents += new_sents
                print('\n\n>> Current Context: {}\nsentence count: ({}/{})\nGenerated: {}'\
                        .format(cur_context, len(sents), num_sentences, new_text))
                cur_context = ' '.join(sents[-2:])

            sample = ' '.join(sents[:num_sentences])
            samples.append(sample)

        return samples


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--checkpoint_path', type=str, required=True, help='trained checkpoint path')
    parser.add_argument("--seed", type=int)
    parser.add_argument("--temperature", type=float, default=1.0)
    parser.add_argument("--num_samples", type=int, default=3)
    parser.add_argument("--batch_size", type=int, default=1)
    parser.add_argument("--top_k", type=int, default=3)
    parser.add_argument("--top_p", type=float, default=.0)

    parser.add_argument("--token_count", type=int, default=384)
    parser.add_argument("--sent_count", type=int, default=10)
    parser.add_argument("--num_sentences", type=int)
    parser.add_argument("--context", type=str, default="")
    args = parser.parse_args()

    model = GPT(args.checkpoint_path,
                args.seed,
                args.temperature,
                args.num_samples,
                args.batch_size,
                args.token_count,
                args.sent_count,
                args.top_k,
                args.top_p)

    out = model.infer(args.context, args.num_sentences)
    print('===== Generated samples =====')
    print('\n\n'.join(out))
