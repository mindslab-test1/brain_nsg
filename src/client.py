import grpc
import argparse
from proto.nsg_pb2 import NsgRequest
from proto.nsg_pb2_grpc import NaturalSentenceGeneratorStub as NsgStub


def request(context, host, port, subject=None, num_sentences=10):
    with grpc.insecure_channel(host+':'+str(port)) as channel:
        stub = NsgStub(channel)
        request = NsgRequest(context=context, subject=subject, num_sentences=num_sentences)
        response = stub.GenerateText(request)
        return response

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', type=str, default="localhost")
    parser.add_argument('--port', type=int, default=36002)
    parser.add_argument('--context', type=str, default="시작하는 텍스트입니다.")
    parser.add_argument('--num_sentences', type=int, default=10)
    args = parser.parse_args()

    nsg_response = request(args.context, args.host, args.port, num_sentences=args.num_sentences)
    print(nsg_response.text)
