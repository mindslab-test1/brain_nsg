import time
import grpc
import logging
import argparse
import json
from concurrent import futures

from gpt_inference import GPT
from proto.nsg_pb2 import NsgResponse
from proto.nsg_pb2_grpc import NaturalSentenceGeneratorServicer
from proto.nsg_pb2_grpc import add_NaturalSentenceGeneratorServicer_to_server as add_servicer_to_server


_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class GptNSG(NaturalSentenceGeneratorServicer):
    def __init__(self, 
                 checkpoint_path,
                 temperature=1.0,
                 token_count=256,
                 sent_count=None,
                 top_k=5,
                 top_p=0.95):

        self.model = GPT(
             checkpoint_path=checkpoint_path,
             temperature=temperature,
             token_count=token_count,
             sent_count=sent_count,
             top_k=top_k,
             top_p=top_p)

        _ = self.model.infer('dummy init inference', num_sentences=5)

    def GenerateText(self, msg, context):
        print('[Server received msg]\n', msg)

        context = (msg.subject + ' ' + msg.context) if msg.subject != '' else msg.context
        _out = self.model.infer(context, msg.num_sentences)
        if type(_out) is list:
            _out = _out[0]
        output = NsgResponse(text=_out)
        print('[response]\n', output)

        return output


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='NSG server arguments')
    parser.add_argument('--port', type=int, default=36002)

    parser.add_argument('--checkpoint_path', type=str, required=True,
                        help='trained checkpoint path')
    parser.add_argument("--temperature", type=float, default=1.0)
    parser.add_argument("--top_k", type=int, default=3)
    parser.add_argument("--top_p", type=float, default=.0)

    parser.add_argument("--token_count", type=int, default=384)
    parser.add_argument("--sent_count", type=int, default=10)
    args = parser.parse_args()

    nsg_servicer = GptNSG(args.checkpoint_path, args.temperature, args.token_count, args.sent_count, args.top_k, args.top_p)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_servicer_to_server(nsg_servicer, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()
    logging.info('nsg starting at 0.0.0.0:%d' % args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
