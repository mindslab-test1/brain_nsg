Natural Sentence Generator
===
GPT2를 활용한 자연어 문장 생성 엔진
- version: 1.0.0
- 예시 코드는 `data/sample` 아래에 뉴스 기사 데이터가 소량 위치한 예시로 작성됐음

## Docker how-to
1. train/test/inference/serve 개발용 이미지
    - 아래 명령어로 도커 실행
    ```
    docker run -it \
        --gpus "\"device={gpu_device}\"" \
        -v {host_pretrained_dir}:/pretrained \
        -v {host_models_dir}:/models \
        --name nsg_engine \
        docker.maum.ai:443/brain/brain_nsg:{version}
    ```
    - 예시(packed image를 사용. 사전학습 체크포인트부터 학습 가능)
    ```
    docker run -it \
        --gpus "\"device=0,2\"" \
        -v models:/models \
        --name nsg_engine \
        docker.maum.ai:443/brain/brain_nsg:1.0.0-packed
    ```
    - 이후 아래 설명에 따라 진행

2. server 전용 도커 이미지 사용 (추가 학습 없이, 학습된 모델을 활용해 서버만 띄울 때)
    ```
    docker run -d \
        --gpus "\"device={gpu_device}\"" \
        -v {host_model_path}:/model \
        -e MODEL_PATH=/model \
        -p {host_port}:{inner_port} \
        -e PORT={inner_port} \
        --name nsg_server \
        docker.maum.ai:443/brain/brain_nsg:{version}-server
    ```
---

### Train
1. Preparation
    - 데이터 형식: `data/` 하위 디렉토리에 각 파일 당 샘플 텍스트 하나씩 파일들을 위치시킨다.
    - 사전학습 모델, vocab 파일을 준비한다. (packed 이미지 포함돼 있음)
    
2. Train script 수정 후 실행 (`scripts/run_train.sh`)
    - `export CUDA_VISIBLE_DEVICES=`: 사용할 GPU 번호 설정(ex. 0,1,2). 맨 앞을 주석으로 처리하면 모든 GPU 사용
    - 스크립트 하단의 설명을 참고하여 argument를 설정한다.
    - 학습 단계에서는 `batch_size`, `accumulate_gradients`, `learning_rate`가 주요
      - GPU 메모리가 충분히 크다면 `batch_size`를 늘리고, OOM(Out Of Memory) error가 뜬다면 `batch_size`를 줄인다.
      - `batch_size`가 너무 작으면 학습이 잘 안될 수 있기 때문에, `batch_size * accumulate_gradients`를 20 이상 되도록 조정한다. 실제 optimizer 적용은 batch별 gradients를 누적해서 진행된다.
      - `batch_size * accumulate_gradients` 값을 키우는 정도에 따라서 `learning_rate`를 조금씩 늘려주면 학습이 더 빨리 진행될 수 있다. 
      - 성능 등의 수치는 데이터에 따라 다르기 때문에, 실험을 진행해야한다.
    - `$ bash scripts/run_train.sh`로 실행

- 학습이 진행되면 step별 loss가 출력된다.
- 학습 후 `models/{model_name}`에 학습된 모델 checkpoint 생성. 

 
### Inference
1. Inference script 수정 후 실행 (`scripts/run_infer.sh`)
    - `export CUDA_VISIBLE_DEVICES=`: 사용할 GPU 번호 설정(ex. 0,1,2). 맨 앞을 주석으로 처리하면 모든 GPU 사용
    - `top_p`, `top_k`가 주요
      - decoding 과정 중 매스텝 후보 토큰들을 어떻게 선정할지를 결정한다.
      - 수치가 클수록 많은 토큰들을 후보로 두며 다양한 표현이 생성되지만, 말이 안되는 문장이 나타나기 쉽다.
      - `top_k`를 설정하면 `top_p`가 override 된다
    - `$ bash scripts/run_test.sh`로 실행


### Serving
1. Server script 수정 후 실행 (`scripts/run_server.sh`)
    - `export CUDA_VISIBLE_DEVICES=`: 사용할 GPU 번호(주석으로 처리하면 모든 GPU 사용)
    - 스크립트 하단의 설명을 참고하여 argument를 설정한다.
    - `$ bash scripts/run_server.sh`로 실행


### Client Demo (Python)
1. Client script 수정 후 실행 (`scripts/run_client.sh`)
    - 스크립트 하단의 설명을 참고하여 argument를 설정한다.
    - `$ bash scripts/run_client.sh`로 실행

### Troubleshoot
1. `$ bash scripts/{}.sh` 실행이 안될 때,
    - 인자를 입력하는 부분에서 `\`뒤에 공백이 없도록 주의

