#!/bin/bash 

# Uncomment below to only utilize specific GPU device(s). 0-indexed.
#export CUDA_VISIBLE_DEVICES=1,5

CKPT="models/sample"                   # use the latest checkpoint
#CKPT="models/sample/model-1000.ckpt"  # use a specific checkpoint
CONTEXT="시작 텍스트입니다. 이 문장을 바꾼 다음 실행하세요."

python src/gpt_inference.py \
    --checkpoint_path "${CKPT}" \
    --context "${CONTEXT}" \
    --token_count 256 \
    --sent_count 8 \
    --num_sentences 20 \
    --num_samples 3 \
    --top_p 0.95 \
    --temperature 1.0 \


# [ Argument usage ]
# 
# --checkpoint_path "path"  : string. 체크포인트 경로. 모델 디렉토리로 하면 최신, 체크포인트까지 설정하면 해당 체크포인트 사용
# --context "시작 텍스트"   : string. 문장을 이어서 생성할 시작 텍스트
# --seed 123                : integer >= 0. random seed. 생략하면 매번 랜덤 생성. 같은 값으로 설정하면 같은 결과 생성 가능
# --token_count 384         : 512 > integer >= 1. 모델 연산 한번에 생성할 최대 토큰 수
# --sent_count 10           : integer >= 1. 모델 연산 한번에 생성할 최대 문장 수
# --num_sentences 20        : integer >= 1. 생성할 텍스트의 최대 문장 수
# --num_samples 5           : integer >= 1. 생성할 총 샘플 텍스트 갯수
# --top_p 0.95              : 1 > float > 0. 클수록 다양한 표현
# --top_k 3                 : integer >= 1. 클수록 다양한 표현. top_k 설정하면 top_p는 무시됨
# --temperature 1.0         : float > 0. 작을수록 다양한 표현
