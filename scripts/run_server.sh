#!/bin/bash 

# Uncomment below to generate grpc-related files under src/proto directory
#bash scripts/build_proto.sh

# Uncomment below to only utilize specific GPU device(s). 0-indexed.
#export CUDA_VISIBLE_DEVICES=1,5

CKPT="models/sample"                   # use the latest checkpoint
#CKPT="models/sample/model-1000.ckpt"  # use a specific checkpoint

python src/server.py \
    --checkpoint_path "${CKPT}" \
    --port 36002 \
    --token_count 384 \
    --sent_count 8 \
    --top_p 0.95 \
    --temperature 1.0 \


# [ Usage ]
#
# --checkpoint_path "path"  : string. 체크포인트 경로. 모델 디렉토리로 하면 최신, 체크포인트까지 설정하면 해당 체크포인트 사용
# --port 36002              : integer >= 0. 서버 port
# --token_count 384         : 512 > integer >= 1. 한번에 생성할 텍스트의 최대 토큰 수 
# --sent_count 10           : integer >= 1. 한번에 생성할 텍스트의 최대 문장 수
# --top_p 0.95              : 1 > float > 0. 클수록 다양한 표현
# --top_k 3                 : integer >= 1. 클수록 다양한 표현. top_k 설정하면 top_p는 무시됨
# --temperature 1.0         : float > 0. 작을수록 다양한 표현
