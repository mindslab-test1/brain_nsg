#!/bin/bash 

# Uncomment below to only utilize specific GPU device(s). 0-indexed.
#export CUDA_VISIBLE_DEVICES=1,5

python src/gpt_trainer.py \
    --run_name "sample2" \
    --dataset "data/sample" \
    --max_step 3000 \
    --save_every 1000 \
    --batch_size 2 \
    --accumulate_gradients 16 \
    --learning_rate 0.0001 \
    --input_length 512 \
    --sample_every 500 \
    --sample_count 5 \
    --sample_length 768 \
    --sample_top_p 0.95 \
    --sample_top_k 3 \


# [ Usage ]
#
# --run_name "run_name"     : string. 실행 이름, 곧 모델명이 된다
# --dataset "path"          : string. 데이터셋 디렉토리 경로
# --max_step 3000           : integer >= 1. 최대 학습 스텝
# --save_every 1000         : integer >= 1. 체크포인트 저장 간격
#
# --batch_size 4            : integer >= 1. batch size.
# --accumulate_gradients 16 : integer >= 1. gradients를 얼마나 누적할지. GPU 메모리가 적은 경우 활용
# --learning_rate 0.0001    : float > 0. learning rate. 0.001 ~ 0.00001 범위 권장
# --noise 0.02              : 1 > float >= 0. noise를 섞을 비율. 오타에 강건한 모델 학습 가능하나 성능 하락 가능
# --input_length            : 512 >= integer >= 1. training data가 얼마만큼 토큰 길이 단위로 잘려져서 들어갈지
#
# --sample_every 500        : integer >= 1. 샘플 생성 간격
# --sample_count 3000       : integer >= 1. 샘플 생성 갯수
# --sample_length 3000      : integer >= 1. 각 샘플 토큰 갯수
# --sample_top_p 0.95       : 1 > float > 0. 클수록 다양한 표현
# --sample_top_k 3          : integer >= 1. 클수록 다양한 표현. top_k 설정하면 top_p는 무시됨
