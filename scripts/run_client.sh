#!/bin/bash 

CONTEXT="시작 텍스트입니다. 이 문장을 바꾼 다음 실행하세요."

python src/client.py \
    --host localhost \
    --port 36002 \
    --num_sentences 10 \
    --context "${CONTEXT}"


# [ Usage ]
#
# --host                    : string. 체크포인트 경로. 모델 디렉토리로 하면 최신, 체크포인트까지 설정하면 해당 체크포인트 사용
# --port 36002              : integer >= 0. 서버 port
# --num_sentences 10        : integer >= 1. 생성할 텍스트의 최대 문장 수
# --context "시작 텍스트"   : string. 문장을 이어서 생성할 시작 텍스트
