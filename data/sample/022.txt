허윤경에 짜릿한 설욕… 변현민, 에쓰오일 챔피언스 우승
우리 챔피언십 '연장 패배' 갚아
한국여자프로골프(KLPGA) 투어 에쓰오일 챔피언스 인비테이셔널(총상금 6억원) 최종 3라운드 18번홀에서 챔피언 퍼트를 집어넣은 뒤 변현민은 울음을 터뜨렸다. 두 달 전 우리투자증권 레이디스 챔피언십에서 연장 끝에 패배한 뒤로 연장 18홀을 도는 꿈을 일주일간 매일 꿨을 만큼 스트레스에 시달렸던 탓이다. 변현민은 그때 우승컵을 안은 동갑내기 친구 허윤경(23)과 이날 챔피언조에서 마지막 홀까지 치열한 우승 경쟁을 벌여 2타 차로 설욕했다.
16일 엘리시안 제주골프장(파72·6575야드)에서 끝난 대회 마지막 3라운드에서 변현민은 버디 8개, 보기 1개로 7타를 줄였다. 최종 합계 17언더파 199타로 이 대회 역대 최소타 기록(200타)을 갈아치우며 우승했다. 2011년 7월 히든밸리 여자오픈 이후 1년11개월 만에 거둔 두 번째 우승이었다.
초등학교 3학년 때 처음 골프를 시작한 변현민은 사업을 하던 아버지가 중2 때 세상을 떠나면서 경제적 어려움을 겪었다. 2007년부터 2부 투어에서 뛰면서 1부 투어 시드전에 세 번 만에 합격했다. 2010년 1부 투어에 데뷔해 10위 안에 한 번도 들지 못하다가 이듬해 깜짝 우승을 차지했다.
변현민은 지난해 상금 랭킹이 52위까지 떨어지며 부진에 빠졌다. 지난겨울 스페인에서 8주간 동계 훈련을 한 것이 전환점이 됐다. 변현민은 "외국 남자 선수들과 함께 훈련하면서 '골프를 즐겨야 더 좋은 결과가 따라온다'는 것을 배웠다"고 했다.