환란때 연대보증 섰던 `억울한 기업인 재활`에 초점
◆ 신용불량 11만명 구제 / 누가 혜택받나 ◆
IMF 외환위기가 낳은 연대보증 피해자 구제는 박근혜 대통령의 서민과 소비자가 중심에 있는 금융을 만들겠다는 대선 공약 이행 '제3탄'이다. 첫 번째가 국민행복기금을 통한 소액 장기 연체자 구제였다면 두 번째는 연대보증 폐지, 세 번째가 바로 IMF 외환위기 연대보증자 채무조정이다. 신제윤 금융위원장이 약속한 '따뜻한 금융'도 같은 맥락에서 이해할 수 있다.
21일 금융위원회가 발표한 외환위기 당시 연대보증 채무자 지원방안을 보면 '억울한' 사람을 구제하겠다는 것이 기본 취지다.
IMF 외환위기로 인한 기업 부도는 개인의 경영실패라기보다는 글로벌 경기 침체, 국가 정책의 실패 등 국가적인 재난으로 볼 수 있는 사태가 가장 큰 원인이다. 이로 인해 억울한 사람들이 양산됐다는 판단에서 지원책 마련이 시작됐다.
실제로 당시 부도율은 기업 경영능력과 상관없이 1996년 0.17%이던 부도율이 1997년 외환위기와 함께 0.52%로 급등한다. 이듬해인 1998년에도 0.52% 수준의 부도율이 유지되다가 1999년에 들어서야 0.43%로 소폭 감소하고 2000년 0.39%와 2001년 0.38%를 거쳐 2002년에 들어서야 부도율이 0.11%로 안정된다.
금융위가 마련한 지원 대상은 연대보증 채무자로 한정했다. 이는 사업 실패의 당사자가 아니라 연대보증 채무로 인해 고통받은 사람들이 더 억울할 수 있다는 판단에서다. 기업 어음이 부도가 나면 해당 기업 임원이라는 이유만으로 '관련인 정보'라는 명목으로 불이익을 받을 수 있는 정보가 은행연합회에 등재됐다.
청와대 관계자는 '과거에는 연대보증이 관행처럼 퍼져 있었다'면서 '그런 상황에서 IMF 외환위기를 맞아 연대보증으로 고통받는 사람들은 반드시 구제해야 한다는 것이 대통령의 뜻'이라고 말했다.
박 대통령은 대선 과정과 인수위원회 시절뿐만 아니라 취임 후 수석비서관회의에서도 IMF 외환위기 당시 신용불량자로 전락한 사람들을 구제할 방안을 찾아야 한다고 언급한 바 있다.
이번 대책의 초점은 재활에 맞춰져 있다. 본인의 잘못이 아닌데도 억울하게 빚의 굴레에서 벗어나지 못한 사람들을 구제하는 선에 그치는 것이 아니라 '패자부활전'의 기회를 주자는 것이다.
연체정보 등 불이익정보 등록자 1104명에 대한 기록을 삭제하기로 한 것은 이 때문이다. 연체된 빚을 다 갚았다 하더라도 과거 연체자였다는 기록 때문에 대출 등 금융거래가 자유롭지 못해 취업이나 창업 등에 애로를 겪어왔다.
채권자가 신청을 하거나 법원이 결정을 내리면 법원 채무불이행자로 등록이 되는데 이는 은행연합회에서 7년간 공공정보로 관리하며 금융거래에 제약을 가한다.
이 중에는 중소기업 연대보증 채무를 부담했다가 주 채무기업이 외환위기로 도산해 연체를 하게 된 사례도 있다. 또 기업어음이 부도가 났을 때 해당 기업 임원이어서 불이익 정보가 등재된 경우도 있다.
따라서 은행연합회 등에 등재된 연체정보가 삭제되면 자유롭게 금융거래를 시작할 수 있고 창업자금 대출 등을 받아 제2의 인생을 시작할 수 있다. 연대보증 채무조정 지원자를 대상으로 정부가 취업과 창업 지원에 나서는 것도 단순 '구제'가 아니라 '재활'에 방점을 찍고 있기 때문이다. 금융위원회는 유관기관과의 협의를 통해 창업 컨설팅, 취업정보 제공 등을 추진하고 필요한 교육 등도 실시할 예정이다.
고용노동부의 취업지원서비스와 중기청의 창업교육, 컨설팅서비스가 우선 고려 대상이다.
장기 연체자 구제와 연대보증 폐지에 이어 향후 금융정책은 서민금융 지원에 무게를 실을 것으로 보인다.