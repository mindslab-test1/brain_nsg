KT, 스마트폰 확산 호재…통신사 평균보다 PER 낮아
국내 통신사 주가는 최근 저조한 흐름을 보이고 있다. KT는 지난 15일 52주 신저가인 3만7850원까지 떨어진 후 횡보하고 있다. 정부의 요금인하 압력과 관련한 불확실성이 주가의 발목을 잡고 있다는 진단이다.
하지만 가입자당 평균 매출(ARPU)이 높은 스마트폰 비중이 지속적으로 증가하고 있는 데다 통신주는 해외 악재의 영향을 적게 받는 대표적인 내수주여서 주가가 곧 제자리를 찾을 것이란 긍정적인 전망이 나온다. 특히 KT는 주가순이익비율(PER)이 다른 통신주들에 비해 낮아 통신주 상승 국면이 오면 탄력이 더 클 것이란 진단이다.
KTB투자증권에 따르면 KT의 올해 예상순이익 기준 PER은 6.4배로 SK텔레콤(7.9배) LG유플러스(16.1배) 등에 비해 저평가된 상태다. 경제협력개발기구(OECD) 30개국 통신사 평균 PER이 14배에 달하는 것과 비교하면 상당히 낮다. 이동전화 보급률은 OECD 30개국이 116%이고,한국은 104%로 경쟁이 더 심한 나라들의 통신사보다 국내 기업들 주가가 저평가된 상태란 설명이다.
최근 KT의 주가에 큰 영향을 끼친 악재 중 하나가 정부의 통신비 인하 시도다. 정부는 물가 인상과 관련,통신비 인하를 유도하기 위해 가격 인가 방식과 통신요금 구조 재검토 등을 추진하는 태스크포스(TF)를 이달 초 가동했다. 통신비 인하로 KT를 비롯한 통신주들의 실적 악화 우려가 제기될수 밖에 없는 상황이다.
하지만 스마트폰 보급 확대로 KT의 주가도 상승세를 곧 찾을 것이란 진단이다. 대신증권은 작년 720만명이었던 스마트폰 인구가 올해는 최대 2000만명까지 늘어날 것으로 예상했다. 김회재 대신증권 연구원은 "ARPU가 높은 스마트폰 확산으로 실적 개선이 진행되고 있다는 점이 1분기 실적에서 확인되면 통신주도 상승세를 탈 것"이라고 관측했다. 김홍식 NH투자증권 연구원은 "통신비 인하는 매년 나오는 이슈인 데다 올해도 인하 수준은 ARPU가 1~2% 내려갔던 예년과 비슷할 것"이라고 진단했다.
KT는 대표적인 배당주로 증시 조정 때 각광받는 주식이기도 하다. 이 회사는 2003년부터 배당성향 50%를 유지하며 순이익의 절반은 주주들에게 환원하고 있다. 작년 결산에서도 주당 2410원을 배당했다. 김회재 연구원은 "올해 예상 순이익 2조3000억원을 기준으로 배당성향 50%를 적용하면 주당 2750원도 가능하다" 고 분석했다.