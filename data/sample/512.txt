2012 차세대CEO 박병엽 팬택 부회장
대기업 틈바구니에서 맨손으로 매출 3조원 규모 회사를 키운 뒤 워크아웃이란 좌절을 맛보고, 다시 기업을 회생시킨 오뚜기 같은 '벤처 신화' 박병엽 팬택 부회장이 매일경제신문이 트랜스미디어 방식으로 실시한 '2012년 차세대 CEO' 설문에서 종합 1위를 차지했다.
박 부회장은 팬택이 워크아웃에 들어간 2007년부터 단 하루도 쉬지 않고 회사를 살리기 위해 고군분투했으며 올 초 심장 스탠스 수술까지 받는 등 큰 건강 부담까지 안고 있다.
그는 1991년 창업해 현대큐리텔과 스카이를 잇달아 인수하며 휴대폰 제조사로 도약했으나 2006년 말 유동성 위기를 맞았다.
재정이 어려운 상황에서도 '스마트폰 올인 전략' 'LTE(롱텀에볼루션) 올인 전략' 등 빠른 의사결정을 했고, 그의 판단은 틀리지 않았다. 스마트폰 2위 강자에 오르고 18분기 연속 흑자를 거두면서 올해 말 워크아웃에서 벗어나 정상 경영 궤도에 올라서게 됐다.
매일경제는 지난해에 이어 올해도 21일부터 27일까지 웹페이지와 스마트폰ㆍ태블릿PC 매경 애플리케이션, QR코드, 소셜댓글 등을 통해 트랜스미디어 방식으로 '2012년 차세대 CEO' 추천을 받았다.
대기업ㆍ중견기업ㆍ금융 분야 69명을 대상으로 추천을 받아 투표 숫자, 댓글 숫자, 소셜미디어 확산 정도 등을 종합적으로 고려해 집계했다.
이 결과 박 부회장에 이어 대기업 부문에선 이부진 호텔신라 사장이 2위에 올랐다. 이 사장은 지난해 말 사장으로 취임한 이후 호텔신라 매출 규모를 20% 이상 키우고 김포공항 면세점의 알짜 사업권을 따내는 등 외형을 키우는 데 성공을 거뒀다. 이 사장은 지난해 차세대 CEO 대기업 부문 1위를 차지한 바 있다.
이어 대기업 부문 3위에는 3세 경영인으로 자리를 잡은 이재용 삼성전자 사장이 올랐다. 중견기업 부문에서는 야구단을 창단하며 스포츠 마케팅에 나선 '리니지 대박 신화' 김택진 엔씨소프트 사장, 모바일 분야에서 약진하고 있는 최세훈 다음커뮤니케이션 사장, 일본 증시에 상장하며 '3조 주식부자'에 등극한 김정주 NXC 회장 등이 1~3위를 차지했다. 지난해에는 신현성 티켓몬스터 대표가 1위에 오른 바 있다.
금융 부문에서는 정태영 현대카드ㆍ현대캐피탈 사장이 지난해에 이어 1위 자리를 지켰다. 정 사장은 현대캐피탈 개인정보 유출 사건 등 위기 속에서도 사용자 입장에서 디자인한 카드를 지속적으로 내놓는 등 경영 능력을 인정받았다.
이어 박현주 미래에셋 회장과 권점주 신한생명 사장이 금융 부문에서 내년 기대되는 차세대 CEO로 뽑혔다.