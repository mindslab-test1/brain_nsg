포스트시즌 최초의 낙동강더비, 이제는 마산이다!
전날 패배를 설욕하는 데 성공한 롯데 자이언츠였다. 준플레이오프 2차전이 열린 사직 야구장은 패배의 설움과 좌절은 잊은 채, 절치부심하는 선수와 팬들이 한 몸이 되어 마치 축제를 보는 듯 했다. 목청껏 소리친 응원에 화답하듯 호수비가 이어졌고 롯데는 팬들에게 승리로 확실한 보답을 했다. 반면 선발투수 장현식의 호투 속에 선전했지만 찬스를 득점으로 연결짓지 못한 NC에게는 답답함의 연속이었다.
이제 공은 부산에서 마산으로 넘어갔다. 양 팀의 시리즈 전적은 1승 1패, 동률이다.
[선발투수 OVERVIEW] 송승준 Vs. 맨쉽 : 관록의 노장이냐 막강 용병이냐
3차전 선발 투수는 각각 롯데는 송승준, NC는 맨쉽으로 정해졌다. 송승준은 이번 시즌 30경기 출장에 11승 5패 1홀드 130.1이닝 동안 4.21의 평균 자책점을 기록했다. 올해로 국내 무대 복귀 11년 차에 달할 만큼 KBO 리그에서는 잔뼈가 굵고, 가을야구 경험도 풍부하다. (10경기 1승 5패 6.33) 하지만 올 시즌 원정 경기에서는 14경기 동안 5승 4패 1홀드에 4.98의 평균 자책점으로 시즌 기록보다는 다소 아쉬운 모습을 보여주고 있다.
올 시즌 NC와의 대결에서는 단 한 번도 선발 등판을 한 적이 없다. 2경기 승패 기록 없이 3.60의 평균 자책점을 기록하고 있다. 5이닝 동안 7개의 탈삼진을 뺏어냈다는 점은 그나마 고무적이다. 특히 이번 포스트시즌 기간 동안 고감도의 타격감을 자랑하는 NC의 권희동과 나성범을 상대로 각각 1차례와 2차례 삼진을 챙겼다는 점은 주목해볼 만 하다. 하지만 1차전에서 만루 홈런을 뽑아내며 시리즈 타율 .400을 기록하고 있는 모창민에게 홈런을 허용한 것이 흠이다. 시즌 초반 컨디션 난조와 더불어 5선발 경쟁에서 밀리며 불펜을 전전하던 와중에 허용한 홈런이라 큰 의미를 부여하긴 어렵지만 상대 전적에서의 우위를 무시할 수는 없는 노릇이다.
NC의 선발 투수 맨쉽은 이번 시즌 21경기 출장에 12승 4패 112.2이닝 동안 3.67의 평균 자책점을 기록했다. 한국 무대와 한국 가을야구는 모두 최초지만 그에게는 작년 월드 시리즈에서 등판했던 경험이 있기에 포스트 시즌을 처음 겪는 투수들과는 출발점부터 다르다. 홈에서는 3.51의 평균 자책점을 기록하며 준수한 모습을 보여주지만 롯데만 만나면 특급 투수가 된다는 점이 핵심이다. 3경기 2승 2.33의 평균 자책점을 기록했고 19.1이닝 동안 20개의 탈삼진을 뽑아냈고 롯데는 3경기 동안 .143의 빈타에 시달리며 말 그대로 롯데 타선에 찬물을 끼얹었다.
완벽에 가까운 그에게도 천적은 있으니, 바로 롯데의 4번 타자인 이대호다. 정규 시즌 상대 전적 .500에 홈런까지 뽑아낼 정도로 맨쉽에겐 그야말로 상대 자체가 청천벽력과도 같은 심정인 셈이다. 번즈도 이에 못지 않은 '맨쉽 저격수'라고 할 수 있다. 타율은 .286로 시즌 타율에 못 미치지만 4안타 중 2루타2개를 허용 할 만큼 껄끄러운 상대였다.
[관전 POINT ①] 팀 공격의 선봉장, 2번 타자를 주목하라
현대 야구에서 주목하고 있는 타순은 바로 2번 타자다. 1번 타자가 출루를 성공하면 이를 진루시킬 수 있는 작전수행능력과 타격능력을 겸비한 2번 타자는 득점기회를 높인다. 게다가 스스로 출루할 수 있는 선구안까지 갖춘 다재다능한 2번타자가 팀에 있을수록 팀의 득점생산력은 높아지기 때문이다.
2차전에서 양 팀이 보여준 공통점은 바로 뜨거웠던 이런 2번 타자들 간의 불방망이 전쟁이었다. 우선 롯데는 우익수 손아섭이 2경기 내내 선발 출장하며(2차전은 3번으로 출장) 3안타 2도루를 기록한 붙박이 2번 타자다. 2루타 1개 또한 기록하며 장타 감각도 여전하다는 점은 NC에게 경계대상 0순위다. 시즌의 대부분을 2번 타순에서 플레이했던 손아섭은 .358/.450/.540의 경이적인 활약을 뽐내며 리그 내에서 가장 무서운 2번 타자로 기세를 드높였다.
문제는 아이러니하게도 그의 타격에 있다. 바로 마산구장에만 가면 차갑게 식어버리는 그의 불방망이 때문인데, 홈에 비해 1할이 넘게 낮아지는 타율(홈 .398 / 원정 .276)이 마산 원정에서는 무려 .200로 폭락한 시즌 기록이 이를 대변해준다. 게다가 NC 상대 타율도 .286으로 꽤나 낮다는 점은 우려스러운 부분이다. 심지어 상대 투수인 맨쉽에게도 2차례의 삼진을 허용했다는 점은 맨쉽에겐 좋은 기억으로 작용할 수 있다.
NC의 2번 타자였던 모창민은 비록 2차전에서는 4타수 1안타로 숨고르기를 했지만 1차전에서는 6타수 3안타에 쐐기 만루 홈런을 뽑아내며 시리즈 성적 .400을 기록하고 있다. 타율과 홈런 등 전반적인 모든 지표에서 커리어 하이를 기록할 만큼 고무적인 시즌을 보내고 있고, 정규시즌의 기세를 고스란히 이어가고 있는 불방망이 또한 매섭기 그지 없다는 점은 롯데가 반드시 경계해야할 부분이다.
기세등등한 모창민에게도 다소 주춤할 여지는 있다. 바로 이번 시즌 통틀어 가장 낮은 컨택 비율을 보인 구종이 바로 스플리터 계열 구종이라는 점이다. 롯데에는 3차전에서 투입 대기할 투수 중에 마무리 손승락을 제외하고 모두 결정구로 스플리터 계열의 구종을 사용한다. 뿐만 아니라 선발 투수 송승준 또한 국내에서는 둘째 가라하면 서러울 만큼 스플리터를 정교하게 구사하는 선수 중 하나인 만큼 떨어지는 공에 헛손질 하는 일에 주의해야할 필요가 있다.
[관전 포인트 ②] 2일 간의 혈투, 지친 롯데의 불펜 vs 휴식을 얻은 NC
양 팀은 이틀 간 20이닝의 혈투를 벌였다. 20이닝 동안 쏟아부은 투수는 NC가 6명인 반면, 롯데는 무려 8명에 달한다. 게다가 롯데는 이미 박진형, 조정훈, 손승락이 연투를 통해 각각 43구, 45구, 49구를 던졌다. 단기전에서 불펜투수들의 체력전은 이미 익히 검증된 사실이다. 하루의 이동일과 짧은 이동거리를 감안한다면 NC의 투수진들과 회복하는 속도는 엇비슷할 수 있으나 투구수가 많은 것은 부인할 수 없는 사실이다.
박진형의 경우 정규 시즌 동안 2연투 5차례, 3연투 3차례를 기록하며 그야말로 마당쇠 역할을 했고, 손승락은 12차례의 2연투와 2차례의 3연투로 2017 시즌 롯데 투혼의 상징으로 자리매김했다. 문제는 이러한 연투 행진이 단기전에서는 누적된 피로로 돌아와 팀에 재를 뿌릴 수 있다는 점이다. 게다가 이들이 무너지면 롯데는 마땅한 대안을 낼 수 없다는 것 또한 문제다. 1차전에서 박시영과 장시환이 제구 난조와 무더기 실책 속에 자멸하며 NC 타선에 처참하게 공략 당한 탓에 남은 시리즈에서 박진형-조정훈-손승락으로 이어지는 불펜 트리오의 호출은 더욱 잦아질 수밖에 없을 전망이다.
롯데의 불펜들이 비지땀을 흘리며 악전고투하는 가운데, NC 투수진은 풍전등화의 상황 속에서 조심스러운 휴식을 맞았다. 장현식의 7이닝 110구, 혼신을 다한 피칭 속에 NC 불펜은 남은 이닝을 고작 2명 9구로 끊어낼 수 있었다. 연투를 이어간 투수도 원종현 혼자 뿐이다. 나머지 투수들은 1차전 해커의 호투 속에 롯데에 비해 한 회 늦게 투입되는 행운을 얻었을 뿐만 아니라 투구수도 대부분 15구 내외로 연투에 있어서 상대적으로 롯데에 비해 안정적인 편이다.
[관전 포인트 ③] 침묵하는 4번 타자, 누가 먼저 터질 것인가
투수전 양상으로 진행되서일까? 양팀의 4번 타자는 선발 투수들의 팔색조와 같은 투구의 향연에 맥을 못추는 기색이 역력하다. 롯데의 4번 이대호는 2경기 동안 .250의 타율을, NC의 4번 스크럭스는 .125의 타율을 보여주고 있다. 게다가 이들은 정규시즌 30+ 홈런을 기록한 타자(이대호 34홈런 / 스크럭스 35홈런)라는 이름값에 비해 가을 나들이에서는 아직 모두 0홈런에 그치고 있다.
단기전에서 홈런을 비롯한 장타는 경기 자체의 분위기를 뒤흔들 수 있다. 양 팀의 핵심 타순인 4번에서 장타를 누가 먼저 뽑아내느냐가 3차전을, 나아가 시리즈 전체를 가져갈 핵심이 될 수 있다.