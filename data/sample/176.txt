그림에 기댄 화(畵)요일(오직 나만 위로하는 그림 전)
그림을 감상한다는 것은 어떤 의미가 있을까. 그림의 예술성, 작가의 의도 등 많은 이유가 있겠고, 또 없을 수도 있다. 별다른 감흥 없이 그냥 그림 한 폭 쭉 훑고 갈 수도 있겠고, 몇 시간이나 그림을 뚫어지게 쳐다볼 수도 있다.
무엇보다 현대회화가 아닌 조선시대 그림을 감상하기란 그리 수월하지 않다. 대부분의 그림이 비슷비슷하기 때문이기도 하겠지만 수묵화의 특별한 차이를 분간해 낼 줄 아는 식견과 안목이 없는 경우가 많기 때문이기도 하겠다.
교과서에 실려 제법 익숙한 ‘세한도’를 보자. 제주도 유배생활을 하면서 그린 추사 김정희의 ‘세한도’는 대부분 선생이 처한 쓸쓸한 처지를 그린 문인화 정도로 알고 있다. 초막에 소나무 세 그루가 전부인 이 그림에서 국보로 지정될 만큼 뛰어난 예술성을 어디에서 찾을까를 고민할 수도 있다.
‘아는 만큼 보인다’는 말은 어느 분야에서나 통용되는 말이지만 특히 그림을 감상할 때 실감하게 된다.
국문학과를 나와 미술사학을 전공한 저자의 그림 해석은 한 편의 시를 읽는 것 같기도 하고, 쉽지만 중요한 철학 강의를 듣는 것 같기도 하다. 어쩌면 곡절 많은 내 인생을 더듬어 보는 것 같기도 한다. ‘세한도’에서 친구의 신의를, ‘모견도’에서 어머니의 깊은 정을, ‘계산포무도’에서 요절한 한 천재의 인생과 고독을, ‘만폭동’에서 물소리를 들려준다.
먼저 저승으로 간 친구를 떠올리며 그린 윤두서의 ‘심득경 초상’에서 저자는 ‘눈물의 근원을 함께 바라본 사이니까, 그런 초상이 그려졌던 것입니다. 그러니 초상을 그리는 화가의 마음이 어떠했을지요. 그 외로움을 딛고, 그래도 그 외로움과 마주하겠다는 심정으로 그렸을 것입니다. 쉬운 일은 아니지요. 외로울 때 있습니다. 세상이 알아주지 않아서, 혹은 세상의 흐름에 무작정 휩쓸리고 싶지 않아서, 서성이며 힘겹습니다’고 말한다.
이 책은 조선을 대표하는 그림 중 24점을 소개하고 있다. 저자 이종수는 먹의 농감이 정갈하고, 담채가 정갈한 우리 옛 그림의 깊이와 거리를 그만의 필채로 담담하게 해석해내고 있다.
어떤 왕이 될 것인가, 스스로를 향한 깊은 고민에 대한 정조의 화답인 ‘야국(野菊)’, 벗 김홍도의 천재성에 가려 ‘2인자’로 살았던 이인문의 아름다움이 빛나는 ‘총석정(叢石亭)’, 두 정인의 달밤 밀회 장면을 달콤하고 알싸하게 그린 신윤복의 ‘월하정인(月下情人)’, 바싹 마른 붓으로 가을의 소리를 스산하게 그려낸 김홍도의 마지막 그림 ‘추성부도(秋聲賦圖)’등을 그만의 독특한 필치로 감상할 수 있다.
자기 내면과의 조우와 화가와의 교감, 미감의 발견, 창작 순간의 내면에 대한 고찰, 그림이 그려진 시대상과 문화상 이해 등 다채로운 통찰의 길을 제공한다.
저자 스스로 말한다. 미술평론가인 존 버거가 말했던 “그림의 기본 목적은 그곳에 없는 그 무엇인가를 불러오는 것이다”고. 한 폭의 그림에서는 도저히 볼 수 없는 무언가를 끄집어낸다. 그림 감상의 또 다른 맛을 안내한다.
이종수 저, 생각정원 간, 1만6000원