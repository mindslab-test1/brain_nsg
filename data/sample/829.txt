민주, “이광재는 억울” 적극 옹호
민주당이 11일 항소심에서 실형을 선고받고 직무정지 위기에 놓인 이광재 강원지사 당선자에 대해 적극 옹호에 나섰다.
이날 오전 항소심 결과가 알려진 직후에는 사법부의 결정을 존중한다는 차원에서 신중한 대응이었지만, 절차와 과정상 문제점이 적지 않다는 판단에서 방향을 바꾼 것으로 보인다.
민주당의 문제의식은 ▲핵심 증인의 출두 의사에도 불구하고 변론재개 신청을 거부한 사법부의 결정 ▲확정 판결 전 무죄추정 원칙 등 크게 두 갈래로 나뉜다.
먼저, 이 당선자측은 증인 출석을 거부해온 박연차 전 태광실업이 출두 의사를 밝혔음에도 불구하고 사법부가 이를 수용하지 않은 것에 반발하고 있다.
이 당선자는 이날 국회에서 기자회견을 갖고 “"박 회장이 법정에 출두하겠다는 의사를 밝혔는데 검찰이 반대 의견을 내면서 법정에서 진실을 밝힐 수 있는 기회가 주어지지 못한 점에 대해서 정말 가슴 아프게 생각한다”고 말했다.
박지원 원내대표도 이날 기자들과 만나 “사법부가 할 역할은 99마리의 양보다 1마리의 길 잃은 양을 찾아주는 것”이라며 “핵심 증인의 심문을 하지 않고 판결한 사법부의 절차에도 문제가 있다고 본다”고 지적했다.
비주류 측의 이종걸 의원도 최근 논평에서 “도민들이 이광재 당선자를 도지사로 선출한 것이라면 재판부는 당연히 이런 국민들의 뜻을 반영하는 재판을 하는 것이 법원칙에 부합하는 판결”이라고 변호한 바 있다.
이 의원은 특히 “일반적으로 헌법학에서 주권자인 국민의 헌법적 결단이 헌법과 동일한 법적효력을 가진다고 평가하기 때문에 이번 선거에서 나타난 국민들의 헌법적 결단을 수용하는 판결이 헌법질서에 합당한 판결이라고 할 것”이라고 법리 공방을 제기하기도 했다.
민주당은 또 2심 재판부의 이번 선고와는 별개로 행정안전부의 지방자치법(111조) 적용 여부에 문제를 제기하고 있다.
율사 출신인 전현희 원내대변인은 지방자치법의 직무정지 관련 조항은 ‘옥중 결재’ 방지가 입법 취지라고 설명한 뒤, 집행유예 판결을 받은 이 당선자에 대해서는 달리 적용해야 한다고 주장했다.
전 대변인은 ‘직무정지’라는 용어도 법률적으로 부정확하다고 지적한 뒤 지방자치법은 직무정지가 아니라 권한대행의 요건이 되는지 유무를 규정하고 있다고 밝혔다.
그는 또 헌법재판소가 판시한 지자체장의 권한대행을 할 수 있는 요건을 거론하며 “만약 이 당선자와 같이 권한대행을 해야 할 아무런 업무상 공백의 구체적 위험이 없는 현 상황에서 권한대행 체제로 간다면, 그것은 헌재에서 금지한 요건에 정면으로 위반되는 것으로 위헌”이라고 말했다.