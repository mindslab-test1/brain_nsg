`재무개선 약정` 기업 늘어난다
금융당국은 주채무계열 선정과 재무개선 약정 체결 대상 기업을 확대하는 등 대기업 부실관리 체계를 대폭 강화할 방침이다. 이에 따라 재무개선 약정을 체결하는 기업이 늘어날 전망이다. 25일 금융위원회에 따르면 부실 위험이 감지된 대기업그룹과 주채권은행이 맺는 재무구조 개선 약정 체결 기준을 재무적 요소뿐만 아니라 비재무적 요소까지 포함해 대폭 강화하기로 했다. 부실화할 가능성이 있는 기업을 사전에 감지해 미리 차단하기 위해서다.
현행 재무구조 개선 약정 체결 기준은 부채비율 대비 유동성과 현금흐름, 영업이익률, 매출액, 이자보상배율 등을 계량화해 선정하고 있다.
이에 따라 현재 주채권은행과 재무구조 개선 약정을 맺는 기업이 늘어날 것으로 전망된다. 현재 동부, 금호아시아나, 성동조선, 대한전선 등 6개 그룹이 재무구조 개선 약정을 맺고 있는데 동양, 현대그룹 등도 포함돼 10여 개 그룹으로 늘어날 것이라는 게 업계 전망이다.
금융위원회 관계자는 '주요 대기업그룹에 대해 사전 부실방지 방안을 강구 중이며 시뮬레이션을 거쳐 연말까지 내놓을 수 있을 것'이라고 밝혔다.
주채권은행의 중점 감시대상인 주채무계열 선정 기준도 대폭 강화하고 주채권은행이 주채무계열에 대해 요구할 수 있는 권한과 징구 서류 등을 구체화한다.
금감원이 최근 주요 은행들과 태스크포스(TF)를 가동해 주채무계열 선정 기준을 현행 0.1% 이상인 신용공여액을 0.075%로 강화하거나 회사채와 CP(기업어음) 등 시장성 부채의 50%를 신용공여에 반영하거나 공정거래법의 규제를 받는 기업집단 중 부채비율이 200% 이상인 기업으로 하는 방안을 금융위에 제출했다.
그러나 금융위원회는 이 같은 내용의 주채무계열 선정 방안을 전면 재검토해 새로운 형태의 대기업집단 사전 부실방지 방안을 마련해 은행업 감독 규정을 개정키로 했다. 현행 제도가 기업의 부실 예방보다는 은행의 부실 최소화에 무게를 두고 있어 근본적인 기업 부실위험 방지에는 부족하다는 판단에서다.