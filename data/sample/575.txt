선진당 '과도' 난동, "염홍철 입당 반대" 외쳐
선진당의 제3기 정치아카데미 수료식장이 난장판이 됐다.
권선택 의원을 지지하는 50대 초반 선진당 당원 2명이 12일 저녁 7시 30분 경 선진당 정치아카데미 수료식이 열린 대전시 둔산동 오페라웨딩홀 4층 행사장에서 칼을 휘두르며 염홍철 전 대전시장의 입당을 반대한다고 밝혔다.
이들 중 한 명은 가슴속에 숨겨 들어온 약 20cm의 과도를 휘두르며 "어떻게 염홍철이 권선택보다 좋냐"며 "백골이 진토 되어 같이 가야 하는 거 아니냐"고 소리쳤다.
또한 염홍철 전 대전시장의 입당이 무르익고 있다는 신문 스크랩을 흔들며 "권선택이 호구냐, 찌르겠다, 건들지 마라"고 행사장 입구 주변 사람들을 협박했다.
사건은 수료식이 끝난 뒤 이회창 총재를 비롯한 선진당 의원 및 선진아카데미 3기 수료생들이 원탁에서 식사를 하고 있던 상황에서 발생했다.
당원들 제지를 받은 이들은 결국 칼을 떨어트렸으나 재차 테이블에 있던 식사용 나이프를 들어 휘두르기까지 했다.
결국 당원들에 의해 오페라웨딩홀 현관 밖으로 쫓겨난 이들은 밑에서 대기하고 있던 일행 두 명과 합류해 이회창 총재가 나오길 기다렸으나 이 총재 일행은 현장을 떠난 뒤였다.
이들은 건물 밖에서도 "다음에는 5백 명, 천명을 데리고 오겠다"며 "이게 무슨 당이냐, (권선택 의원이) 당이 힘들 때 들어와서 충청권 총선 승리에 기여하고 다른 의원들까지 도와줬는데 염홍철 입당이 말이 되느냐"며 분을 삭이지 못했다.
현장에서 이들의 행동을 제지하던 권선택 의원은 "염홍철 전 시장의 입당을 반대하는 당원들"이라고 밝혔다.
권선택 의원은 실내가 정돈된 뒤 마이크를 잡고 "당원인데 약주 한 잔 한 거 같다. 과격한 행동에 대해 제가 대신 미안하다는 말을 드린다"고 사과했다.
권 의원은 염홍철 전 대전시장, 이상민 의원과 함께 선진당 대전시장 주요 후보로 거론되고 있다.
한편, 경찰 관계자는 "보고 받았다"며 "상황을 조금 더 파악 한 뒤 수사 할 것"이라고 말해 지방선거를 앞둔 선진당의 이미지 추락이 불가피 할 것으로 보인다.