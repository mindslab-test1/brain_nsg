전국여행상품 다 모았다
황금연휴 기간을 겨냥해 숙박부터 체험권까지 전국 여행상품을 한곳에 모은 온라인 사이트가 운영된다.
SK플래닛 11번가가 문화체육관광부와 한국관광공사가 정한 '가을관광주간'에 맞춰 큐레이션 커머스 '쇼킹딜11시'를 통해 50여 개 여행상품을 판매하는 '전국 방방곳곳 가을여행 기획전'을 10월 한 달간 선보인다.
11번가는 개천절, 한글날 등 10월 황금연휴를 맞아 11번가 여행전문MD가 엄선한 관광지 입장권, 체험권, 숙박권 등을 쇼킹딜11시를 통해 판매한다. 특별히 이번 기획전을 위해 지역 특산물과 문화를 체험할 수 있는 여행상품을 강화했다.
가족과 연인을 위한 '대관령 양떼목장과 커피거리' 당일여행은 2만4900원에, 담양 '죽녹원+메타세콰이어+전주한옥마을' 패키지는 2만8000원에 판매한다.
또 강원 '설악산 자유 단풍여행'은 1만9000원, '주왕산 주선지 단풍여행'은 2만7900원에 내놨다. 홍천, 설악, 단양, 변산, 거제, 양양 등 대명리조트 숙박권은 8만1000원부터다.