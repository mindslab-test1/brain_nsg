수능 학력평가 문제지 유출 안양 교사 2명 직위해제
경기도교육청은 지난해 6월 전국연합학력평가 문제지 등을 학원가에 유출한 안양 모 공립고교 A교사를 직위해제했다고 1일 밝혔다.
또 같은 학력평가 문제지 등을 7차례에 걸쳐 유출했다고 인정한 안양 관내 모 사립고교 B교사에 대해서도 학교법인에 직위해제를 요구할 방침이다.
두 교사의 징계 문제에 대해서는 경찰의 수사결과가 나온 뒤 논의하기로 했다.
도교육청은 지난달 28일 시험문제 유출과 관련한 경찰의 수사개시 통보를 받고 곧바로 해당 2개 학교에 대한 조사를 벌였다.
조사에서 A교사의 경우 6월 한국교육과정평가원 주관 전국연합학력평가를 비롯해 지난해 모두 3차례에 걸쳐 전국단위 연합학력평가 문제지 및 답안지를 평가 당일 인근 학원 원장에게 유출한 사실을 확인했다.
B교사는 2011년 3월부터 지난해 6월까지 모두 7차례에 걸쳐 역시 전국연합학력평가 문제지를 같은 학원장에게 미리 보낸 것을 밝혀냈다.
도내에서는 지난해 교육과정평가원 및 각 시·도교육청 주관으로 모두 6차례 연합학력평가가 치러졌다.
도교육청 조사에서 두 교사는 관련 학원장이 평소 알고 지내던 사람이며, 학원 수강생 지도를 위해 필요하다고 해 문제지 등을 미리 줬다고 진술했다.
이들은 그러나 조사 과정에서 문제지 유출에 따른 금품 등 대가는 받지 않았다고 밝힌 것으로 전해졌다.
도교육청은 문제지 유출 사실이 밝혀짐에 따라 조만간 연합학력평가 보안 지침을 강화하는 등 유사 사건 재발을 막기 위한 대책을 마련하겠다고 밝혔다.