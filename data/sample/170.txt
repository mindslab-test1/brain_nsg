주유소 휘발유 5주째 고공행진…연중최고치 경신
국내 휘발유 가격이 5주 연속 치솟으며 또다시 연중 최고치를 갈아치웠다.
4일 한국석유공사 유가 정보 서비스인 '오피넷'에 따르면 8월 첫째주 전국 주유소에서 판매된 보통 휘발유 가격은 전주보다 ℓ당 평균 1.7원 상승한 1614.0원으로 나타났다.
지난 6월 넷째주 이후 줄곧 상승곡선을 이어가면서 연중 최고치를 한 주 만에 다시 경신했다. 이는 2014년 12월 넷째주(1620.0원) 이후 약 3년 8개월 만에 가장 높은 가격이다. 최근 3년간 최저점이었던 2016년 3월 둘째주(1340.4원)와 비교하면 20.4%나 오른 셈이다.
자동차용 경유도 전주보다 1.6원 오른 1414.9원으로, 역시 2014년 12월 넷째 주(1431.3원) 이후 최고치를 기록하면서 올해 들어 가장 높은 가격에 판매됐다. 실내 등유도 944.5원으로 0.9원 올랐다.
상표별로는 알뜰주유소의 휘발유 가격이 1591.7원으로 전주보다 2.0원이나 올랐으며, 가장 비싼 SK에너지도 1.9원 상승한 1630.7원을 기록했다.
지역별로는 최고가 지역인 서울의 휘발유 가격이 평균 1.8원 오른 1698.6원으로 1700원 선에 근접했다. 대구가 1.5원 오른 1588.9원으로 가장 낮은 가격에 판매된 것으로 나타났다.
같은 기간 주요 정유사의 휘발유 공급 가격은 '월말 효과'에 따라 전주보다 ℓ당 14.5원 내린 1520.4원으로 조사됐다. 경유와 등유도 각각 13.3원과 17.2원 하락한 1334.5원과 825.5원에 공급됐다.